puppet-module-neutron (25.0.0-2) unstable; urgency=medium

  * Add designate-region_name-param.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Dec 2024 12:20:27 +0100

puppet-module-neutron (25.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Oct 2024 11:10:55 +0200

puppet-module-neutron (24.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed versions of depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Apr 2024 16:59:14 +0200

puppet-module-neutron (23.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed versions of depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Oct 2023 13:57:06 +0200

puppet-module-neutron (22.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Jun 2023 11:24:54 +0200

puppet-module-neutron (22.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed runtime depends versions for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 12 Apr 2023 10:55:06 +0200

puppet-module-neutron (21.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Removed templates from installed folders.

 -- Thomas Goirand <zigo@debian.org>  Thu, 20 Oct 2022 23:19:39 +0200

puppet-module-neutron (20.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2022 12:14:30 +0200

puppet-module-neutron (20.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 26 Mar 2022 10:02:11 +0100

puppet-module-neutron (20.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Mar 2022 00:02:16 +0100

puppet-module-neutron (19.4.0-2) unstable; urgency=medium

  * Clean-up update-alternatives handling.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Jan 2022 10:16:30 +0100

puppet-module-neutron (19.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Oct 2021 10:01:23 +0200

puppet-module-neutron (19.3.0-1) unstable; urgency=medium

  * Uploading to unstable.
  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 14:52:00 +0200

puppet-module-neutron (19.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Removed patch applied upstream:
    - Add_support_for_neutron_api_uwsgi_config_in_Debian.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Sep 2021 14:41:09 +0200

puppet-module-neutron (18.4.0-2) unstable; urgency=medium

  * Add Add_support_for_neutron_api_uwsgi_config_in_Debian.patch.
  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 17:27:05 +0200

puppet-module-neutron (18.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 06 Apr 2021 13:29:44 +0200

puppet-module-neutron (18.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 22:00:04 +0100

puppet-module-neutron (18.2.0-1) experimental; urgency=medium

  * New usptream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 22 Mar 2021 17:48:13 +0100

puppet-module-neutron (17.5.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 16:17:02 +0200

puppet-module-neutron (17.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Oct 2020 09:42:27 +0200

puppet-module-neutron (17.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Oct 2020 13:56:34 +0200

puppet-module-neutron (16.3.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 11:42:27 +0200

puppet-module-neutron (16.3.0-2) experimental; urgency=medium

  * Add configuring_neutron-api-uwsgi.ini.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 May 2020 22:11:21 +0200

puppet-module-neutron (16.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 03 May 2020 13:42:18 +0200

puppet-module-neutron (16.2.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 26 Apr 2020 12:38:15 +0200

puppet-module-neutron (15.4.0-3) unstable; urgency=medium

  * debhelper-compat (= 11).
  * Standards-Version: 4.5.0.
  * Depends on puppet, not puppet-common.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Mar 2020 10:55:43 +0100

puppet-module-neutron (15.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Oct 2019 00:30:07 +0200

puppet-module-neutron (15.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Oct 2019 11:20:42 +0200

puppet-module-neutron (15.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 29 Sep 2019 11:42:19 +0200

puppet-module-neutron (14.4.0-3) unstable; urgency=medium

  * Switched build-dependencies to Python 3 (Closes: #937363).

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Sep 2019 21:09:31 +0200

puppet-module-neutron (14.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Jul 2019 22:01:23 +0200

puppet-module-neutron (14.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed runtime depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Apr 2019 10:40:48 +0200

puppet-module-neutron (13.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Using team+openstack@tracker.debian.org for maintainer field.
  * Fixed versions of depends: for this release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Jun 2018 16:20:06 +0200

puppet-module-neutron (12.3.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol
  * d/control: Set Vcs-* to salsa.debian.org

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Deprecating priority extra as per policy 4.0.1.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed depends for this release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Mar 2018 14:28:03 +0100

puppet-module-neutron (9.4.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed runtime depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 21 Oct 2016 13:38:44 +0000

puppet-module-neutron (8.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)

  [ Thomas Goirand ]
  * Fixed missing runtime depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 29 Apr 2016 10:37:56 -0500

puppet-module-neutron (8.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Overrides dh_auto_install and dh_auto_build to avoid reno using git at
    build time.
  * Added python-pbr, python-setuptools and python-all as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Apr 2016 08:28:45 +0000

puppet-module-neutron (8.0.0~b1-1) experimental; urgency=medium

  * New upstrem release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2016 18:13:39 +0100

puppet-module-neutron (7.0.0-1) unstable; urgency=medium

  * Initial release. (Closes: #814243)

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Feb 2016 17:01:57 +0800
