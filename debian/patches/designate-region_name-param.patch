Author: Thomas Goirand <zigo@debian.org>
Date: Mon, 02 Dec 2024 12:12:55 +0100
Description: designate region_name param
 The class neutron::designate didn't have a region_name parameter.
 In our deployment, this was an issue we had to fix.
Change-Id: Ifde81be654ca73cb88515804b38a6e053f1409ac
Forwarded: https://review.opendev.org/c/openstack/puppet-neutron/+/936823
Last-Update: 2024-12-02

diff --git a/manifests/designate.pp b/manifests/designate.pp
index 3ad4dff..13096c8 100644
--- a/manifests/designate.pp
+++ b/manifests/designate.pp
@@ -50,6 +50,10 @@
 #   (optional) Required if identity server requires client certificate
 #   Defaults to $facts['os_service_default'].
 #
+# [*region_name*]
+#   (Optional) The region in which the identity server can be found.
+#   Defaults to $facts['os_service_default'].
+#
 # [*allow_reverse_dns_lookup*]
 #   (optional) Enable or not the creation of reverse lookup (PTR) records.
 #   Defaults to $facts['os_service_default'].
@@ -78,6 +82,7 @@
   $auth_url                  = 'http://127.0.0.1:5000',
   $cafile                    = $facts['os_service_default'],
   $certfile                  = $facts['os_service_default'],
+  $region_name               = $facts['os_service_default'],
   $allow_reverse_dns_lookup  = $facts['os_service_default'],
   $ipv4_ptr_zone_prefix_size = $facts['os_service_default'],
   $ipv6_ptr_zone_prefix_size = $facts['os_service_default'],
@@ -107,6 +112,7 @@
     'designate/auth_url':                  value => $auth_url;
     'designate/cafile':                    value => $cafile;
     'designate/certfile':                  value => $certfile;
+    'designate/region_name':               value => $region_name;
     'designate/allow_reverse_dns_lookup':  value => $allow_reverse_dns_lookup;
     'designate/ipv4_ptr_zone_prefix_size': value => $ipv4_ptr_zone_prefix_size;
     'designate/ipv6_ptr_zone_prefix_size': value => $ipv6_ptr_zone_prefix_size;
diff --git a/releasenotes/notes/designate-region-name-102d02ddc000de3f.yaml b/releasenotes/notes/designate-region-name-102d02ddc000de3f.yaml
new file mode 100644
index 0000000..ce7579c
--- /dev/null
+++ b/releasenotes/notes/designate-region-name-102d02ddc000de3f.yaml
@@ -0,0 +1,4 @@
+---
+features:
+  - |
+    The ``neutron::server`` class now has a new ``region_name`` parameter.
diff --git a/spec/classes/neutron_designate_spec.rb b/spec/classes/neutron_designate_spec.rb
index 9273804..43df1ee 100644
--- a/spec/classes/neutron_designate_spec.rb
+++ b/spec/classes/neutron_designate_spec.rb
@@ -25,6 +25,7 @@
         should contain_neutron_config('designate/auth_url').with_value('http://127.0.0.1:5000')
         should contain_neutron_config('designate/cafile').with_value('<SERVICE DEFAULT>')
         should contain_neutron_config('designate/certfile').with_value('<SERVICE DEFAULT>')
+        should contain_neutron_config('designate/region_name').with_value('<SERVICE DEFAULT>')
         should contain_neutron_config('designate/allow_reverse_dns_lookup').with_value('<SERVICE DEFAULT>')
         should contain_neutron_config('designate/ipv4_ptr_zone_prefix_size').with_value('<SERVICE DEFAULT>')
         should contain_neutron_config('designate/ipv6_ptr_zone_prefix_size').with_value('<SERVICE DEFAULT>')
@@ -43,6 +44,7 @@
           :auth_url                  => 'http://localhost:5000',
           :cafile                    => '/path/to/cafile',
           :certfile                  => '/path/to/certfile',
+          :region_name               => 'region2',
           :allow_reverse_dns_lookup  => false,
           :ipv4_ptr_zone_prefix_size => 765,
           :ipv6_ptr_zone_prefix_size => 876,
@@ -63,6 +65,7 @@
         should contain_neutron_config('designate/auth_url').with_value('http://localhost:5000')
         should contain_neutron_config('designate/cafile').with_value('/path/to/cafile')
         should contain_neutron_config('designate/certfile').with_value('/path/to/certfile')
+        should contain_neutron_config('designate/region_name').with_value('region2')
         should contain_neutron_config('designate/allow_reverse_dns_lookup').with_value(false)
         should contain_neutron_config('designate/ipv4_ptr_zone_prefix_size').with_value(765)
         should contain_neutron_config('designate/ipv6_ptr_zone_prefix_size').with_value(876)
